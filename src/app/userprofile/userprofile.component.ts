import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {UserserviceService} from "../userservice.service";
import {FollowingService} from "../following.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
  usersdata;
  username;
  email;
  userid;
  useradded;
  blo;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private userservice: UserserviceService, private http: HttpClient, private followingservice: FollowingService) { }

  ngOnInit() {
    // this.route.paramMap.subscribe((param: ParamMap) => {
    //   this.username = param.get('username');
    //   console.log(this.username);
    //   // console.log("User id is");
    //   // console.log(this.userid);
    //   this.userservice.getuserbyusername(this.username).subscribe( data => {
    //     this.usersdata = data;
    //     // this.username = this.usersdata.username;
    //     this.email = this.usersdata.email;
    //     this.userid = this.usersdata.id;
    //
    //     console.log(this.usersdata.email);
    //     console.log(this.usersdata);
    //   });
    //
    // });
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    this.http.get('http://localhost:2019/profile/getall',{headers}).subscribe(data => {
      this.blo = data;
      console.log(data);
    });
  }
    follow(id) {
      console.log(id);
      this.userservice.followuser(id).subscribe( data => {
        this.useradded = data;
        this.followingservice.isfollowing(true);
      });


    }
}
