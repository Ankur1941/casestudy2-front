import { Component, OnInit } from '@angular/core';
import {UserserviceService} from "../userservice.service";
import {FollowingService} from "../following.service";

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss']
})
export class MyprofileComponent implements OnInit {
  my;
  useradded;

  constructor(private user: UserserviceService, private followingservice: FollowingService) {
  }

  ngOnInit() {
    this.user.showuser().subscribe(data => {
      this.my = data;
    });
  }

  // follow() {
  //   console.log(this.my.id);
  //   this.user.followuser(this.userid).subscribe(data => {
  //     this.useradded = data;
  //     this.followingservice.isfollowing(true);
  //   });
  // }
}
