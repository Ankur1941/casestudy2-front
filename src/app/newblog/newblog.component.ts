import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-newblog',
  templateUrl: './newblog.component.html',
  styleUrls: ['./newblog.component.scss']
})
export class NewblogComponent implements OnInit {
name;
details;
description;
image;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  newsave() {
    if (this.description != null && this.name != null && this.details != null && this.image != null) {
      const token = sessionStorage.getItem('token');
      const headers = new HttpHeaders({Authorization: 'Basic ' + token});
      return this.http.post('http://localhost:2019/blogs/addBlog', {
        name: this.name,
        details: this.details,
        description: this.description,
        image: this.image
      }, {headers}).subscribe(data => {
        console.log(data);
      });
    } else {
      alert('One or More Fields are Empty. Please Complete the form');
    }
  }

}
