import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {UserserviceService} from "../userservice.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-updateblog',
  templateUrl: './updateblog.component.html',
  styleUrls: ['./updateblog.component.scss']
})
export class UpdateblogComponent implements OnInit {
mypro;
Prod;
name;
details;
description;
image;
  constructor(private route: ActivatedRoute, private Service: UserserviceService, private http: HttpClient) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      // tslint:disable-next-line:radix
      const id = parseInt(params.get('id'));
      this.mypro = id;
    });
    this.Service.getblogfromid(this.mypro).subscribe((data) => {
      this.Prod = data;
      this.name = this.Prod.name;
      this.details = this.Prod.details;
      this.description = this.Prod.description;
      this.image = this.Prod.image;
      console.log(this.Prod);
    });
  }
  savechanges() {
    console.log("insidw savechanges");
    if (this.name != null && this.details != null && this.description != null && this.image != null) {
      console.log("inside if");
      const token = sessionStorage.getItem('token');
      const headers = new HttpHeaders({Authorization: 'Basic ' + token});
      return this.http.put('http://localhost:2019/blogs/updateblog/' + this.Prod.id, {
        name: this.name,
        details: this.details,
        description: this.description,
        image: this.image
      }, {headers}).subscribe(data => {
        console.log(data);
      });
    } else
    {
      alert('One or more Fields are Empty');
    }
  }


}
