import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  constructor(private client: HttpClient) {
  }

  showuser() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    const url = 'http://localhost:2019/profile/getprofile';
    return this.client.get(url, {headers});
  }
  getuserbyusername(username) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.client.get('http://localhost:2019/user/allusers/' + username, {headers});
  }
  getblogfromid(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    const url = 'http://localhost:2019/blogs/get/' + id;
    return this.client.get(url, {headers});
  }

  // updateuser(id, pdata: Userdata) {
  //   const token = sessionStorage.getItem('token');
  //   const headers = new HttpHeaders({Authorization: 'Basic ' + token});
  //   return this.http.put('http://localhost:8080/user/updateuser/' + id, pdata, {headers});
  // }
  followuser(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.client.get('http://localhost:2019/f/new/UserId/' + id, {headers});
  }

  Displayfollowing() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.client.get('http://localhost:2019/f/exp', {headers});
  }

  unfollowuser(userid) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.client.get('http://localhost:2019/f/delete/UserId/' + userid, {headers});

  }
}
