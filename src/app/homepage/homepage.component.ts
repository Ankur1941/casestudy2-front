import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UserserviceService} from "../userservice.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
blo;
pro;
  constructor(private http: HttpClient, private userservice: UserserviceService) { }

  ngOnInit() {


    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    this.http.get('http://localhost:2019/blogs/getbuserid',{headers}).subscribe(data => {
      this.blo = data;
      console.log(data);
    });
  }
    Deleteblog(id)
    {
      console.log("delete inittiated");
      const token = sessionStorage.getItem('token');
      const headers = new HttpHeaders({Authorization: 'Basic ' + token});
      return this.http.get('http://localhost:2019/blogs/delete/UserId/' + id, {headers}).subscribe(data => {
        console.log(data);
      });
    }
}
