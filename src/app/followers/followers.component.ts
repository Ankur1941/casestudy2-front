import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {UserserviceService} from '../userservice.service';
import {FollowingService} from '../following.service';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {
  followinglist;
  deletionsring;

  constructor(private http: HttpClient, private usersservice: UserserviceService, private followservice: FollowingService) { }

  ngOnInit() {
    this.usersservice.Displayfollowing().subscribe(data => {
      this.followinglist = data;
      console.log(data);
    });

  }
  unfollow(userid) {
    this.usersservice.unfollowuser(userid).subscribe( data => {
      this.deletionsring = data;
      this.followservice.isfollowing(false);
    });
  }



}
