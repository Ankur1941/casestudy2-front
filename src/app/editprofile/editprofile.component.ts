import { Component, OnInit } from '@angular/core';
import {UserserviceService} from "../userservice.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthenticationService} from "../authentication.service";

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {
email;
confirm;
password;
contact;
address;
  constructor(private user : UserserviceService, private http: HttpClient, private auth: AuthenticationService) { }
my;
  ngOnInit() {
    this.user.showuser().subscribe(data => {
      this.my = data;
      this.email = this.my.email;
      this.password = this.my.password;
      this.contact = this.my.contact;
      this.address = this.my.address;
      console.log(this.my);
    });
  }
  savechanges()
  {console.log("inside sAVECHANGES" + this.password + this.contact + this.address + this.email);
  console.log("inside if");
  if(this.password!=null && this.contact!=null && this.confirm!=null && this.password!=null && this.address!=null && this.email!=null) {
    if (this.password == this.confirm) {
      const token = sessionStorage.getItem('token');

      const headers = new HttpHeaders({Authorization: 'Basic ' + token});
      return this.http.put('http://localhost:2019/profile/updateuser/' + this.my.id, {
        email: this.email,
        password: this.password,
        address: this.address,
        contact: this.contact
      }, {headers}).subscribe(data => {
        console.log(data);
        this.auth.logoutService();
        alert("Profile Changes Saved. Logging out Please Login Again");
      });
    }
    else
    {
      alert("Password is not same as Confirm Password");
    }
  }
  else {
    alert("one or more Fields are Empty");
  }

  }

}
