import {Router, Routes} from "@angular/router";
import {HomepageComponent} from "./homepage/homepage.component";
import {SignupComponent} from "./signup/signup.component";
import {LoginComponent} from "./login/login.component";
import {MyprofileComponent} from "./myprofile/myprofile.component";
import {EditprofileComponent} from "./editprofile/editprofile.component";
import {NewblogComponent} from "./newblog/newblog.component";
import {UpdateblogComponent} from "./updateblog/updateblog.component";
import {FollowingService} from "./following.service";
import {FollowersComponent} from "./followers/followers.component";
import {UserprofileComponent} from "./userprofile/userprofile.component";

// tslint:disable-next-line:variable-name
export const Main_Routes: Routes = [
  {  path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'home', component: HomepageComponent,
  },
  {
    path: 'signup', component: SignupComponent,
  },
 {
    path: 'login', component: LoginComponent,
  },
 {
    path: 'profile', component: MyprofileComponent,
  },
 {
    path: 'editprofile', component: EditprofileComponent,
  },
  {
    path: 'newblog', component: NewblogComponent,
  },
 {
    path: 'updateblog/:id', component: UpdateblogComponent,
  },
  {
    path: 'userprofile',
    component: UserprofileComponent
  },
  {
    path: 'following', component: FollowersComponent,
  }

];
