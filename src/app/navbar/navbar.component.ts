import { Component, OnInit } from '@angular/core';
import {AppService} from "../app.service";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {AuthenticationService} from "../authentication.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  logouturl;

  // tslint:disable-next-line:max-line-length
  constructor(private service: AppService, private router: Router, private authService: AuthenticationService, private client: HttpClient) {
  }

  ngOnInit() {
  }

  logout() {
    if (this.authService.isUserLoggedIn()) {
      this.authService.logoutService();
      // this.client.get(this.logouturl).subscribe(res => {
      //   alert('logout Successful');
      // });
      this.router.navigate(['login']);
    }
  }

  checklogin() {
    return this.service.checklogin();
  }
navi()
{
  this.router.navigate(['/following']);
}
  gotofollowing() {
    this.router.navigate(['/following']);

  }
  navi1()
  {
    this.router.navigate(['/userprofile'])
  }

}
